# Koombea Contacts

The application will allow users to upload contact files in CSV format and process them in order to generate a unified contact file. The contacts must be associated to the user who imported them into the platform. When uploading the files, the application must validate that the fields entered are correctly formatted. You must take into account that the files can have many records.

### Dependencies and versions

- Ruby version 3.0.1
- NodeJS version 16.4.0
- Yarn version 1.22.4
- Postgres version 13
- Redis >= 3.3.5
- Docker 20.10.7

### Development env setup

1. Postgres database

    1.1. Docker:

    Create the docker volume:
    ```
    $ docker volume create localhost-pgdata
    ```

    Run the container:
    ```
    $ docker run --name postgres-localhost --rm -p 5432:5432 -v localhost-pgdata:/var/lib/postgresql/data -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -d postgres:13-alpine
    ```

2. Redis

    1.1. Docker:

    Run the container:
    ```
    $ docker run --rm --name localhost-redis -p 6379:6379 -d redis:6.2-alpine redis-server --appendonly yes
    ```

3. Running the app locally

    2.1. Get Ruby dependencies
    ```
    $ bundle install
    ```

    2.2. Get Javascript dependencies
    ```
    $ yarn install
    ```

    3.1 Setup app database:
    ```
    $ rails db:setup
    ```

### Tests

1. Run:
    ```
    $ rspec
    ```

### Missing (lack of time)

- Add feature/integration tests;
- Unit tests;
- Add pagination to the interface;
