require 'rails_helper'

RSpec.describe Contact, type: :model do
  let(:user) { User.create(email: "user@domain.net", password: "123456") }
  let(:contact) { Contact.new(contact_attrs) }

  let(:contact_errors) { 
    contact.valid?
    contact.errors
  }

  let(:valid_attrs) {
    {
      name: "Fugiat Reprehenderit Nisi",
      date_of_birth: "19841230",
      telephone: "(+57) 320-432-05-09",
      address: "Reprehenderit cillum exercitation eiusmod cillum",
      credit_card: "4111111111111111",
      email: "user@domain.net",
      user_id: user.id
    }
  }

  context "when all attributes are valid" do
    it { expect(Contact.new(valid_attrs)).to be_valid }
  end

  context "when name is invalid" do
    let(:contact_attrs) { valid_attrs.merge!({name: "$up3r N0m3"}) }

    it { expect(contact_errors[:name].first).to eq "name MUST not have special characters, EXCEPT '-'"}
  end

  context "when name is nil" do
    let(:contact_attrs) { valid_attrs.merge!({name: nil}) }

    it { expect(contact_errors[:name].first).to eq "can't be blank" }
  end

  context "when email is invalid" do
    let(:contact_attrs) { valid_attrs.merge!({email: "user@_.com"}) }

    it { expect(contact_errors[:email].first).to eq "must have a valid email format" }
  end

  context "when user alread have the email contact" do
    before do
      Contact.create(valid_attrs)
    end

    let(:contact_attrs) { valid_attrs }

    it { expect(contact_errors[:email].first).to eq "user already has this email contact" }
  end

  context "when telephone is invalid" do
    let(:contact_attrs) { valid_attrs.merge!({telephone: "+57 320 432 05 09"}) }

    it { expect(contact_errors[:telephone].first).to eq "must have a valid telephone format. Ex: (+57) 320 432 05 09 or (+57) 320-432-05-09" }
  end

  context "when date of birth is invalid" do
    let(:contact_attrs) { valid_attrs.merge!({date_of_birth: "1984/12/30"}) }

    it { expect(contact_errors[:date_of_birth].first).to eq "invalid date format. Date must be ISO 8601, formats: %Y%m%d or %F" }
  end

  context "when address is invalid" do
    let(:contact_attrs) { valid_attrs.merge!({address: nil}) }

    it { expect(contact_errors[:address].first).to eq "can't be blank" }
  end

  context "when the credit card number is a Visa" do
    let(:contact_attrs) { valid_attrs }

    it "has the credit card franchise attribute defined properly" do
      expect(contact.credit_card_franchise).to eq "Visa"
    end
  end

  context "when the credit card number is correct" do
    let(:contact_attrs) { valid_attrs }

    it "has the last 4 numbers of credit card number set properly" do
      expect(contact.last_4_credit_card).to eq "1111"
    end
  end
end
