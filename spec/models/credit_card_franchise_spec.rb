require 'rails_helper'

RSpec.describe CreditCardFranchise do
  describe ".by_number" do
    subject { CreditCardFranchise.by_number(number) }

    context "when card number is from American Express" do
      let(:number) { "371449635398431" }
      it { is_expected.to eq("American Express") }
    end

    context "when card number is from Diners Club" do
      let(:number) {"30569309025904"}
      it { is_expected.to eq("Diners Club") }
    end

    context "when card number is from Discover" do
      let(:number) {"6011111111111117"}
      it { is_expected.to eq("Discover") }
    end

    context "when card number is from JCB" do
      let(:number) {"3530111333300000"}
      it { is_expected.to eq("JCB") }
    end

    context "when card number is from MasterCard" do
      let(:number) {"5555555555554444"}
      it { is_expected.to eq("MasterCard") }
    end

    context "when card number is from Visa" do
      let(:number) {"4111111111111111"}
      it { is_expected.to eq("Visa") }
    end

    context "when card number is from Diners Club" do
      let(:number) {"3003078706376917"}
      it { is_expected.to eq("Diners Club") }
    end

    context "when card number is from Discover" do
      let(:number) {"6011399811450212"}
      it { is_expected.to eq("Discover") }
    end
  end
end
