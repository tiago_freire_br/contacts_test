require 'rails_helper'

RSpec.describe "contacts/show", type: :view do
  before(:each) do
    @contact = assign(:contact, Contact.create!(
      name: "Name",
      telephone: "Telephone",
      address: "Address",
      credit_card: "",
      credit_card_franchise: "Credit Card Franchise",
      email: "Email"
    ))
  end

  # it "renders attributes in <p>" do
  #   render
  #   expect(rendered).to match(/Name/)
  #   expect(rendered).to match(/Telephone/)
  #   expect(rendered).to match(/Address/)
  #   expect(rendered).to match(//)
  #   expect(rendered).to match(/Credit Card Franchise/)
  #   expect(rendered).to match(/Email/)
  # end
end
