require 'rails_helper'

RSpec.describe "contacts/new", type: :view do
  before(:each) do
    assign(:contact, Contact.new(
      name: "MyString",
      telephone: "MyString",
      address: "MyString",
      credit_card: "",
      credit_card_franchise: "MyString",
      email: "MyString"
    ))
  end

  it "renders new contact form" do
    render

    assert_select "form[action=?][method=?]", contacts_path, "post" do

      assert_select "input[name=?]", "contact[name]"

      assert_select "input[name=?]", "contact[telephone]"

      assert_select "input[name=?]", "contact[address]"

      assert_select "input[name=?]", "contact[credit_card]"

      assert_select "input[name=?]", "contact[credit_card_franchise]"

      assert_select "input[name=?]", "contact[email]"
    end
  end
end
