require 'rails_helper'

RSpec.describe "contacts/index", type: :view do
  before(:each) do
    assign(:contacts, [
      Contact.create!(
        name: "Name",
        telephone: "Telephone",
        address: "Address",
        credit_card: "",
        credit_card_franchise: "Credit Card Franchise",
        email: "Email"
      ),
      Contact.create!(
        name: "Name",
        telephone: "Telephone",
        address: "Address",
        credit_card: "",
        credit_card_franchise: "Credit Card Franchise",
        email: "Email"
      )
    ])
  end

  # it "renders a list of contacts" do
  #   render
  #   assert_select "tr>td", text: "Name".to_s, count: 2
  #   assert_select "tr>td", text: "Telephone".to_s, count: 2
  #   assert_select "tr>td", text: "Address".to_s, count: 2
  #   assert_select "tr>td", text: "".to_s, count: 2
  #   assert_select "tr>td", text: "Credit Card Franchise".to_s, count: 2
  #   assert_select "tr>td", text: "Email".to_s, count: 2
  # end
end
