require 'rails_helper'

RSpec.describe "ContactsFiles", type: :request do
  before do
    user = create(:user)
    sign_in user
  end

  describe "GET /contacts_files" do
    it "returns http success" do
      pending("Pending")
      visit "/contacts_files/new"
      get "/contacts_files"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /contacts_files/1" do
    it "returns http success" do
      pending("Pending")
      get "/contacts_files/1"
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST /contacts_files" do
    it "returns http created (201)" do
      pending("Pending")
      post "/contacts_files"
      expect(response).to have_http_status(:created)
    end
  end
end
