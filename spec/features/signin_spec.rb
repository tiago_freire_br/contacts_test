require 'rails_helper'

feature "Sign in" do
  background do
    User.create(email: 'user@email.net', password: '123456')
  end

  scenario "Sign in with valid data" do
    visit '/users/sign_in'

    within("#new_user") do
      fill_in "Email", with: "user@email.net"
      fill_in "Password", with: "123456"
    end

    click_button "Log in"
    expect(page).to have_content "Contacts"
    expect(page).to have_content "Log out"
  end

  scenario "Sign in with invalid password" do
    visit '/users/sign_in'

    within("#new_user") do
      fill_in "Email", with: "user@email.net"
      fill_in "Password", with: "1234567"
    end

    click_button "Log in"
    expect(page).to have_content "Invalid Email or password"
  end

  scenario "Sign in with invalid email" do
    visit '/users/sign_in'

    within("#new_user") do
      fill_in "Email", with: "user@email.com"
      fill_in "Password", with: "123456"
    end

    click_button "Log in"
    expect(page).to have_content "Invalid Email or password"
  end
end