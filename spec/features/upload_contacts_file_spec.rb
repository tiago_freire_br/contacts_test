require 'rails_helper'

feature "Upload contacts files", type: :feature do
  scenario "when the data is correct" do
    user = create(:user)
    sign_in user

    visit "/contacts_files/new"

    page.attach_file("contacts_file_csv_file", Rails.root + 'spec/factories/contacts_file.csv', visible: false)

    fill_in "contacts_file_file_attributes_map[name_column_index]", with: 0
    fill_in "contacts_file_file_attributes_map[date_of_birth_column_index]", with:1
    fill_in "contacts_file_file_attributes_map[phone_column_index]", with: 2
    fill_in "contacts_file_file_attributes_map[address_column_index]", with: 3
    fill_in "contacts_file_file_attributes_map[credit_card_column_index]", with: 4
    fill_in "contacts_file_file_attributes_map[email_column_index]", with: 5

    click_button "Save"

    expect(page).to have_content("contacts_file.csv")
    expect(page).to have_content("On hold")
  end
end