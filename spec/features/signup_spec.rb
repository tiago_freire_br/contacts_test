require 'rails_helper'

feature "Sign up" do
  scenario "Sign up with valid data" do
    visit '/users/sign_up'

    within("#new_user") do
      fill_in "Email", with: "user@email.net"
      fill_in "Password", with: "123456"
    end

    click_button "Sign up"
    expect(page).to have_content "Contacts"
    expect(page).to have_content "Log out"
  end
end