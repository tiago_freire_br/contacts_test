require 'rails_helper'

feature "User's uploaded contacts files", type: :feature do
  scenario "when the list is empty" do
    user = create(:user)
    sign_in user
    
    visit "/contacts_files"

    expect(page).to have_content("The list is empty")
  end
end