class Contact < ApplicationRecord
  attr_accessor :credit_card, :raw_birthdate

  belongs_to :user, inverse_of: :contacts

  validates :name, :email, :address, :telephone, :date_of_birth, :credit_card_franchise, presence: true
  validates_uniqueness_of :email, scope: :user_id, message: "user already has this email contact"

  validates :name, format: { with: /\A[a-zA-Z0-9\s\-]+\z/,
                   message: "name MUST not have special characters, EXCEPT '-'" }

  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP,
                   message: "must have a valid email format" }

  validates :telephone, format: { with: /\A\(\+57\)(\s|-)[0-9]{3}(\s|-)[0-9]{3}(\s|-)[0-9]{2}(\s|-)[0-9]{2}\z/,
                   message: "must have a valid telephone format. Ex: (+57) 320 432 05 09 or (+57) 320-432-05-09" }

  validate :birthdate_format

  def credit_card=(credit_card_number)
    return if credit_card_number.nil?
    write_attribute(:credit_card_digest, digest_from_number(credit_card_number))
    write_attribute(:last_4_credit_card, credit_card_number[-4..-1])
    write_attribute(:credit_card_length, credit_card_number.length)
    write_attribute(:credit_card_franchise, CreditCardFranchise.by_number(credit_card_number))
  end

  def date_of_birth=(value)
    write_attribute(:date_of_birth, value)
    @raw_birthdate = value
  end

  def digest_from_number(credit_card_number)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost

    BCrypt::Password.create(credit_card_number, cost: cost)
  end

  private

  def birthdate_format
    if !date_of_birth_valid?
      errors.add :date_of_birth, "invalid date format. Date must be ISO 8601, formats: %Y%m%d or %F"
    end

  end

  def date_of_birth_valid?
    return false if @raw_birthdate.nil?

    Date._strptime(@raw_birthdate, '%Y%m%d').present? ||
    Date._strptime(@raw_birthdate, '%F').present?
  end
end
