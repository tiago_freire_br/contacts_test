class ContactsFile < ApplicationRecord
  serialize :file_attributes_map, Hash
  serialize :file_errors, Hash

  enum status: [:on_hold, :processing, :failed, :finished]

  has_one_attached :csv_file
  belongs_to :user, inverse_of: :contacts_files

  scope :by_user, ->(user) { where(user_id: user.id) }
end
