class CreditCardFranchise
  # Ref: Active Merchant gem
  # https://github.com/activemerchant/active_merchant/blob/b32d571a60569aa4c2998fbad8448e45361c8a7b/lib/active_merchant/billing/credit_card_methods.rb#L4

  CARD_COMPANY_DETECTORS = {
    'Visa'               => ->(num) { num =~ /^4\d{12}(\d{3})?(\d{3})?$/ },
    'MasterCard'             => ->(num) { num&.size == 16 && in_bin_range?(num.slice(0, 6), MASTERCARD_RANGES) },
    'Elo'                => ->(num) { num&.size == 16 && in_bin_range?(num.slice(0, 6), ELO_RANGES) },
    'Alelo'              => ->(num) { num&.size == 16 && in_bin_range?(num.slice(0, 6), ALELO_RANGES) },
    'Discover'           => ->(num) { num =~ /^(6011|65\d{2}|64[4-9]\d)\d{12,15}|(62\d{14,17})$/ },
    'American Express'   => ->(num) { num =~ /^3[47]\d{13}$/ },
    'Naranja'            => ->(num) { num&.size == 16 && in_bin_range?(num.slice(0, 6), NARANJA_RANGES) },
    'Diners Club'        => ->(num) { num =~ /^3(0[0-5]|[68]\d)\d{11,16}$/ },
    'JCB'                => ->(num) { num =~ /^(35(28|29|[3-8]\d)\d{12}|308800\d{10})$/ },
    'Dankort'            => ->(num) { num =~ /^5019\d{12}$/ },
    'Maestro'            => lambda { |num|
      (12..19).cover?(num&.size) && (
        in_bin_range?(num.slice(0, 6), MAESTRO_RANGES) ||
        MAESTRO_BINS.any? { |bin| num.slice(0, bin.size) == bin }
      )
    },
    'Forbrugsforeningen' => ->(num) { num =~ /^600722\d{10}$/ },
    'Sodexo'             => ->(num) { num =~ /^(606071|603389|606070|606069|606068|600818)\d{10}$/ },
    'Alia'               => ->(num) { num =~ /^(504997|505878|601030|601073|505874)\d{10}$/ },
    'VR'                 => ->(num) { num =~ /^(627416|637036)\d{10}$/ },
    'Cabal'              => ->(num) { num&.size == 16 && in_bin_range?(num.slice(0, 8), CABAL_RANGES) },
    'China UnionPay'     => ->(num) { (16..19).cover?(num&.size) && in_bin_range?(num.slice(0, 8), UNIONPAY_RANGES) },
    'Carnet'             => lambda { |num|
      num&.size == 16 && (
        in_bin_range?(num.slice(0, 6), CARNET_RANGES) ||
        CARNET_BINS.any? { |bin| num.slice(0, bin.size) == bin }
      )
    },
    'Olimpica' => ->(num) { num =~ /^636853\d{10}$/ },
    'Creditel' => ->(num) { num =~ /^601933\d{10}$/ },
    'Confiable' => ->(num) { num =~ /^560718\d{10}$/ }
  }

  # http://www.barclaycard.co.uk/business/files/bin_rules.pdf
  ELECTRON_RANGES = [
    [400115],
    (400837..400839),
    (412921..412923),
    [417935],
    (419740..419741),
    (419773..419775),
    [424519],
    (424962..424963),
    [437860],
    [444000],
    [459472],
    (484406..484411),
    (484413..484414),
    (484418..484418),
    (484428..484455),
    (491730..491759),
  ]

  CARNET_RANGES = [
    (506199..506499),
  ]

  CARNET_BINS = Set.new(
    %w[
      286900 502275 606333 627535 636318 636379 639388
      639484 639559 50633601 50633606 58877274 62753500
      60462203 60462204 588772
    ]
  )

  # https://www.mastercard.us/content/dam/mccom/global/documents/mastercard-rules.pdf, page 73
  MASTERCARD_RANGES = [
    (222100..272099),
    (510000..559999),
    [605272],
    [606282],
    [637095],
    [637568],
    (637599..637600),
    [637609],
  ]

  MAESTRO_BINS = Set.new(
    %w[ 500057
        501018 501043 501045 501047 501049 501051 501072 501075 501087 501089 501095
        501500
        501879 502113 502301 503175
        503645 503670
        504310 504338 504363 504533 504587 504620 504639 504656 504738 504781 504910
        507001 507002 507004 507082 507090
        560014 560565 561033
        572402 572610 572626
        576904
        578614
        581149
        585274 585697
        586509
        588729 588792
        589244 589407 589471 589605 589633 589647 589671
        590043 590206 590263 590265 590278 590361 590362 590379 590393 590590
        591235 591420 591481 591620 591770 591948 591994
        592024 592161 592184 592186 592201 592384 592393 592528 592566 592704 592735 592879 592884
        593074 593264 593272 593355 593496 593556 593589 593666 593709 593825 593963 593994
        594184 594409 594468 594475 594581 594665 594691 594710 594874 594968
        595355 595364 595532 595547 595561 595568 595743 595929
        596245 596289 596399 596405 596590 596608 596645 596646 596791 596808 596815 596846
        597077 597094 597143 597370 597410 597765 597855 597862
        598053 598054 598395 598585 598793 598794 598815 598835 598838 598880 598889
        599000 599069 599089 599148 599191 599310 599741 599742 599867
        601070 601638
        604983
        606126
        636380 636422 636502 636639
        637046 637756
        639130 639229
        690032]
  )

  # https://www.mastercard.us/content/dam/mccom/global/documents/mastercard-rules.pdf, page 73
  MAESTRO_RANGES = [
    (500032..500033),
    (501015..501016),
    (501020..501021),
    (501023..501029),
    (501038..501041),
    (501053..501058),
    (501060..501063),
    (501066..501067),
    (501080..501083),
    (501091..501092),
    (501104..501105),
    (501107..501108),
    (501104..501105),
    (501107..501108),
    (561200..561269),
    (561271..561299),
    (561320..561356),
    (581700..581751),
    (581753..581800),
    (589998..591259),
    (591261..596770),
    (596772..598744),
    (598746..599999),
    (600297..600314),
    (600316..600335),
    (600337..600362),
    (600364..600382),
    (601232..601254),
    (601256..601276),
    (601640..601652),
    (601689..601700),
    (602011..602050),
    (639000..639099),
    (670000..679999),
  ]

  # https://dev.elo.com.br/apis/tabela-de-bins, download csv from left sidebar
  ELO_RANGES = [
    506707..506708, 506715..506715, 506717..506722, 506724..506736, 506739..506743,
    506745..506747, 506753..506753, 506774..506778, 509000..509007, 509009..509014,
    509020..509030, 509035..509042, 509044..509089, 509091..509101, 509104..509807,
    509831..509877, 509897..509900, 509918..509964, 509971..509986, 509995..509999,
    627780..627780, 636368..636368, 650031..650033, 650035..650051, 650057..650081,
    650406..650439, 650485..650504, 650506..650538, 650552..650598, 650720..650727,
    650901..650922, 650928..650928, 650938..650939, 650946..650978, 651652..651704,
    655000..655019, 655021..655057
  ]

  # Alelo provides BIN ranges by e-mailing them out periodically.
  # The BINs beginning with the digit 4 overlap with Visa's range of valid card numbers.
  # By placing the 'alelo' entry in CARD_COMPANY_DETECTORS below the 'visa' entry, we
  # identify these cards as Visa. This works because transactions with such cards will
  # run on Visa rails.
  ALELO_RANGES = [
    402588..402588, 404347..404347, 405876..405876, 405882..405882, 405884..405884,
    405886..405886, 430471..430471, 438061..438061, 438064..438064, 470063..470066,
    496067..496067, 506699..506704, 506706..506706, 506713..506714, 506716..506716,
    506749..506750, 506752..506752, 506754..506756, 506758..506767, 506770..506771,
    506773..506773, 509015..509019, 509880..509882, 509884..509885, 509887..509887,
    509987..509992
  ]

  CABAL_RANGES = [
    60420100..60440099,
    58965700..58965799,
    60352200..60352299
  ]

  NARANJA_RANGES = [
    589562..589562
  ]

  # In addition to the BIN ranges listed here that all begin with 81, UnionPay cards
  # include many ranges that start with 62.
  # Prior to adding UnionPay, cards that start with 62 were all classified as Discover.
  # Because UnionPay cards are able to run on Discover rails, this was kept the same.
  UNIONPAY_RANGES = [
    81000000..81099999, 81100000..81319999, 81320000..81519999, 81520000..81639999, 81640000..81719999
  ]

  def self.in_bin_range?(number, ranges)
    bin = number.to_i
    ranges.any? do |range|
      range.include?(bin)
    end
  end

  # Returns a string containing the brand of card from the list of known information below.
  def self.by_number(number)
    CARD_COMPANY_DETECTORS.each do |company, func|
      return company.dup if func.call(number)
    end

    return nil
  end
end
