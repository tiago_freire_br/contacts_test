require 'csv'

class ContactsCsvProcessorJob < ApplicationJob
  queue_as :default

  def perform(contacts_file_id)
    contacts_file = ContactsFile.find(contacts_file_id)

    if contacts_file
      contacts_file.processing!

      csv_file = contacts_file.csv_file.download
      csv = CSV.parse(csv_file)

      any_valid_contact = false

      attr_map = contacts_file.file_attributes_map
      name_column = attr_map['name_column_index'].to_i
      birthdate_column = attr_map['date_of_birth_column_index'].to_i
      phone_column = attr_map['phone_column_index'].to_i
      address_column = attr_map['address_column_index'].to_i
      credit_card_column = attr_map['credit_card_column_index'].to_i
      email_column = attr_map['email_column_index'].to_i

      csv.each_with_index do |row, line|
        contact_attrs = {
          name: row[name_column],
          date_of_birth: row[birthdate_column],
          telephone: row[phone_column],
          address: row[address_column],
          credit_card: row[credit_card_column],
          email: row[email_column],
          user: contacts_file.user
        }

        contact = Contact.new(contact_attrs)

        if contact.save
          any_valid_contact = true
        else
          contacts_file.file_errors[line] = contact.errors.full_messages
        end
      end

      any_valid_contact ? contacts_file.finished! : contacts_file.failed!
    end
  end
end
