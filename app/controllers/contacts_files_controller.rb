class ContactsFilesController < ApplicationController
  before_action :authenticate_user!

  def index
    @contacts_files = ContactsFile.by_user(current_user).order(:id).page params[:page]
  end

  def new
    @contacts_file = ContactsFile.new
  end

  def create
    @contacts_file = ContactsFile.new(contacts_file_params)
    @contacts_file.user = current_user
    @contacts_file.on_hold!

    respond_to do |format|
      if @contacts_file.save
        ContactsCsvProcessorJob.perform_later(@contacts_file.id)

        format.html { redirect_to @contacts_file, notice: "Contacts file was successfully saved." }
        format.json { render :show, status: :created, location: @contacts_file }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @contacts_file.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @contacts_file = ContactsFile.find(params[:id])
  end

  private

  ATTR_MAP_KEYS = %i[name_column_index date_of_birth_column_index phone_column_index address_column_index credit_card_column_index email_column_index]

  def contacts_file_params
    params.require(:contacts_file).permit(:csv_file, file_attributes_map: ATTR_MAP_KEYS)
  end
end
