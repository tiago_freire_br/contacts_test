json.extract! contact, :id, :name, :date_of_birth, :telephone, :address, :credit_card, :credit_card_franchise, :email, :created_at, :updated_at
json.url contact_url(contact, format: :json)
