class CreateContacts < ActiveRecord::Migration[6.1]
  def change
    create_table :contacts do |t|
      t.string :name, null: false
      t.date :date_of_birth, null: false
      t.string :telephone, null: false
      t.string :address, null: false
      t.string :credit_card_digest, null: false
      t.string :last_4_credit_card, null: false
      t.integer :credit_card_length, null: false
      t.string :credit_card_franchise, null: false
      t.string :email, null: false

      t.belongs_to :user, null: false, foreign_key: true

      t.timestamps
    end

    add_index :contacts, [:user_id, :email], unique: true
  end
end
