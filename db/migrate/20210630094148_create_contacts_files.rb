class CreateContactsFiles < ActiveRecord::Migration[6.1]
  def change
    create_table :contacts_files do |t|
      t.integer :status, null: false, default: 0
      t.json :file_errors
      t.json :file_attributes_map, null: false
      t.belongs_to :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
